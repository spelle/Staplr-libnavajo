##############################
# EXAMPLE                    #
# created by T.DESCOMBES     #
#                    2015    #
##############################

LIBNAVAJO_DIR = /usr/local/libnavajo
OUTPUTFILENAME = PrecompiledRepository.cc
NAVAJO_PRECOMPILER_EXEC = $(LIBNAVAJO_DIR)/bin/navajoPrecompiler

UNAME := $(shell uname)
LBITS := $(shell getconf LONG_BIT)

ifeq ($(UNAME), Linux)
OS = LINUX
else ifeq ($(UNAME), Darwin)
OS = MACOSX
else
OS = OTHER
endif

DLOPEN_PAM=0
ifeq ($(DLOPEN_PAM),1)
        LIBPAM=-ldl
else
        LIBPAM=-lpam
endif

LIB_DIR      = lib
CXX 	=  g++

ifeq ($(OS),MACOSX)
LIBSSL_DIR = /usr/local/Cellar/openssl/1.0.1j
LIBS       = -lnavajo -L$(LIBNAVAJO_DIR)/$(LIB_DIR) -lz  -L$(LIBSSL_DIR)/lib -lssl -lcrypto $(LIBPAM)
DEFS            =   -D__darwin__ -D__x86__ -fPIC -fno-common -D_REENTRANT
CXXFLAGS        =  -O3  -Wdeprecated-declarations
else
ifeq ($(LBITS),64)
  LIB_DIR=lib64
endif
LIBS       = -lnavajo -L$(LIBNAVAJO_DIR)/$(LIB_DIR) -lz -lssl -lcrypto -pthread $(LIBPAM)
DEFS            =  -DLINUX -Wall -Wno-unused -fexceptions -fPIC -D_REENTRANT
CXXFLAGS        =  -O4  -Wdeprecated-declarations
endif


CPPFLAGS	= -I. \
                  -I$(LIBSSL_DIR)/include \
		  -I$(LIBNAVAJO_DIR)/include

LD		=  g++

LDFLAGS        =  -Wall -Wno-unused -O3   

APP_NAME     = staplr

APP_OBJS = \
		  PrecompiledRepository.o \
		  $(APP_NAME).o


#######################
# DEPENDENCE'S RULES  #
#######################

%.o: %.cc
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(CPPFLAGS) $(DEFS) 
	
all: $(APP_NAME)

PrecompiledRepository.o:
	@echo Generate Cpp files from HTML repository
	@rm -f $(OUTPUTFILENAME)
	@find . \( -name "*~" -o -name "*.old" -o -name "*.bak" \) -exec rm -f '{}' \;
	$(NAVAJO_PRECOMPILER_EXEC) $(APP_NAME)Repository >> $(OUTPUTFILENAME) ; cd ..
	$(CXX) -c PrecompiledRepository.cc -o $@ $(CXXFLAGS) $(CPPFLAGS) $(DEFS)
	
$(APP_NAME): $(APP_OBJS) $(LIB_STATIC_NAME)
	rm -f $@
	$(LD) $(LDFLAGS) -o $@ $(APP_OBJS) $(LIB_STATIC_NAME) $(LIBS) 

clean:
	@rm -f $(OUTPUTFILENAME) 
	@for i in $(APP_OBJS); do  rm -f $$i ; done






